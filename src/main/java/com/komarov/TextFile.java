package com.komarov;

import sun.nio.cs.UTF_32;

import java.io.*;
import java.nio.charset.Charset;

public class TextFile extends MyFile {

    public TextFile(String path) {
        super(path);
    }

    @Override
    public void log() {
        try {
            FileInputStream fileInputStream = new FileInputStream(getPath());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream, Charset.forName("windows-1251")));
            String line;
            System.out.println("\nСодержимое файла:\n***********************");
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
            System.out.println("***********************");
        } catch (IOException e) {
            //e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
}
