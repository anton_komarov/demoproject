package com.komarov;

public class ImageFile extends MyFile {

    public ImageFile(String path) {
        super(path);
    }

    @Override
    public void log() {
        System.out.println("Невозможно вывести содержимое выбранного файла!");
    }
}
