package com.komarov;

public class OtherFile extends MyFile {

    public OtherFile(String path) {
        super(path);
    }

    @Override
    public void log() {
        System.out.println("Разрешение выбранного файла не \"png\" и не \"txt\", выберите другой файл");
    }
}
