package com.komarov;

import java.io.File;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите путь к папке с файлами: ");
        String folderPath = in.nextLine();
        File folder = new File(folderPath);
        File[] listOfFiles = folder.listFiles();

        while (true) {
            System.out.println("Выберите файл, который необходимо открыть: ");
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    System.out.println((i+1) + ") " + listOfFiles[i].getName());
                } else if (listOfFiles[i].isDirectory()) {
                    System.out.println((i + 1) + ") " + listOfFiles[i].getName() + " (папка)");
                }
            }
            System.out.print("\nВыбранный файл под номером: ");

            int k = in.nextInt()-1;
            while ((k < 0) || (k > listOfFiles.length-1) || (listOfFiles[k].isDirectory())) {
                System.out.println("Нельзя выбирать папки (а также введенное число должно быть от 1 до " + listOfFiles.length + ")");
                System.out.println("Выберите файл снова");
                k = in.nextInt() - 1;
            }
            String name = listOfFiles[k].getName();
            String path = listOfFiles[k].getPath();
            MyFile myFile;
            if (".txt".equals(name.substring(name.lastIndexOf('.')))) {
                myFile = new TextFile(path);
            } else if (".png".equals(name.substring(name.lastIndexOf('.')))) {
                myFile = new ImageFile(path);
            } else {
                myFile = new OtherFile(path);
            }
            myFile.log();
            System.out.println("\nВведите любое число, чтобы продолжить\n0) Выход");
            if (in.nextInt() == 0) break;
        }
        in.close();
    }
}
